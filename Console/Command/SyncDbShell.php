<?php
App::uses("AppShell", "Console/Command");

/**
 * Class SyncDbShell
 * @property DB DB
 */
class SyncDbShell extends AppShell {

    public $uses = array('Izzdb.DB');

    public function main() {
        $this->out("<info>Hello there, since you executed the main method, now we will update almost all your database - hold on - this can take a couple minutes</info>\n");
        $this->materialized_views();
        $this->versions();
        $this->functions();
        $this->procedures();
        $this->triggers();
        $this->views();
        $this->out("\n<info>Well done, if you want to update your materialized views, or insert any kind of dummy data, you have to execute this command again caling the right methods.</info>");
    }

    public function help(){
        $this->out("<info>====> Command Options:</info>",1, Shell::NORMAL);
        $this->out("<warning>init:</warning>                        <info>create all tables and sample content</info>",1, Shell::NORMAL);
        $this->out("<warning>views:</warning>                       <info>drop/create all your views</info>",1, Shell::NORMAL);
        $this->out("<warning>procedures:</warning>                  <info>drop/create all your procedures</info>",1, Shell::NORMAL);
        $this->out("<warning>functions:</warning>                   <info>drop/create all your functions</info>",1, Shell::NORMAL);
        $this->out("<warning>triggers:</warning>                    <info>drop/create all your triggers</info>",1, Shell::NORMAL);
        $this->out("<warning>materialized_views:</warning>          <info>drop/create all your materialized views</info>",1, Shell::NORMAL);
        $this->out("<warning>dummy_data [cardNumber]:</warning>     <info>insert dummy data for a specific case</info>",1, Shell::NORMAL);
        $this->out("<warning>versions:</warning>                    <info>execute all sql files generated for each change made in this system</info>",1, Shell::NORMAL);
        $this->out("<warning>custom_folder [folderName]:</warning>  <info>execute all sql files inside the custom folder</info>",1, Shell::NORMAL);
        $this->out("<warning>help:</warning>                        <info>show this message</info>",1, Shell::NORMAL);
        $this->out("\n<info>Sample: </info>");
        $this->out("\t<warning>Console/cake Izzdb.sync_db</warning> <error>force</error>");
        $this->out("\t<warning>Console/cake Izzdb.sync_db versions</warning> <error>force</error>");
        $this->out("\t<warning>Console/cake Izzdb.sync_db custom_folder [folderName]</warning> <error>force</error>");

    }

    public function views(){
        $oldValue = Configure::read("Izzdb.explode");
        Configure::write("Izzdb.explode", true);
        $messages = $this->DB->sync_db("views");
        Configure::write("Izzdb.explode", $oldValue);
        $this->printMessages($messages, "views");
    }

    public function procedures(){
        $messages = $this->DB->sync_db("procedures");
        $this->printMessages($messages, "procedures");
    }

    public function functions(){
        $messages = $this->DB->sync_db("functions");
        $this->printMessages($messages, "functions");
    }

    public function triggers(){
        $messages = $this->DB->sync_db("triggers");
        $this->printMessages($messages, "triggers");
    }

    public function materialized_views(){
        $messages = $this->DB->sync_db("materialized_views");
        $this->printMessages($messages, 'materialized_views');
    }

    public function dummy_data(){
        if(empty($this->args[0])){
            $this->out("Error: You must inform what card do you wanna execute like: syncdb dummy_data cardNumber",1, Shell::NORMAL);
            return false;
        }
        $messages = $this->DB->sync_db("dummy_data".DS.$this->args[0]);
        $this->printMessages($messages, 'dummy_data ' . $this->args[0]);
    }

    public function versions(){
        $oldValue = Configure::read("Izzdb.explode");
        Configure::write("Izzdb.explode", true);
        $messages = $this->DB->sync_db("versions");
        Configure::write("Izzdb.explode", $oldValue);
        $this->printMessages($messages, 'versions');
    }

    public function custom_folder(){
        if(empty($this->args[0])){
            $this->out("Error: You must inform what folder do you wanna execute like: syncdb custom_folder folder_name",1, Shell::NORMAL);
            return false;
        }
        $messages = $this->DB->sync_db($this->args[0]);
        $this->printMessages($messages, 'folders');
    }

    public function init(){
        $oldValue = Configure::read("Izzdb.explode");
        Configure::write("Izzdb.explode", true);
        $messages = $this->DB->sync_db("skeleton");
        Configure::write("Izzdb.explode", $oldValue);
        $this->printMessages($messages, "init");
    }

    public function printMessages($messages, $folder_name){
        $this->stdout->outputAs(ConsoleOutput::COLOR);
        if(empty($messages)){
            $this->out("<info>Updating {$folder_name}:</info> OK", 1, Shell::NORMAL);
        } else {
            $this->out("<info>Updating {$folder_name}:</info> Errors/Warnings - See below", 1, Shell::NORMAL);
        }

        foreach($messages as $message){
            if(strpos($message, 'Error') !== false){
                $tag = 'error';
            } else {
                $tag = 'warning';
            }
            $this->out("<{$tag}>{$message}</{$tag}>", 1, Shell::NORMAL);
        }

    }
}
