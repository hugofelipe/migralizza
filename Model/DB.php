<?php
App::uses("Model", "Model");
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Class DB
 */
class DB extends Model
{

    public $useTable = false;

    public $actsAs = [];

    public $messages = [];


    public function __construct($id = false, $table = null, $ds = null){
        $this->delete_models_cache();
        parent::__construct($id, $table, $ds);

    }

    public function query($query, $folder_name = '', $file_name = ''){
        $error = '';
        // Try to execute the specific query
        $ds = $this->getDataSource();
        $ds->begin();
        try {
            $result = parent::query($query);
            $ds->commit();
        } catch(Exception $e){
            $ds->rollback();
            $result = false;
            $error = $e;
        }

        // Throw an exception to be handled by this class
        if(!empty($error)){
            throw $error;
        }
        return $result;

    }

    public function delete_models_cache(){
        $path = CACHE . 'models'.DS;
        $tmp = new Folder($path);
        $caches = $tmp->find('.*.');

        foreach($caches as $cache){
            unlink($path.$cache);
        }
    }

    public function sync_db($scope = "")
    {
        $this->messages = [];
        if (empty($scope)) {
            return false;
        }

        return $this->execute_queries_from($scope);

    }

    /**
     * @param string $folder_name
     *
     * @return array
     */
    public function execute_queries_from($folder_name)
    {
        $paths = $this->get_paths();

        foreach ($paths as $key => $path) {
            $files = $this->get_sql_files($path, $folder_name);
            foreach ($files as $sql) {
                $content = $this->get_content_from_sql_file($path, $folder_name, $sql);
                if ($this->do_we_need_to_explode()) {
                    $this->handle_multiple_sql_treaments($path, $folder_name, $sql, $content);
                } else {
                    $this->handle_sql_treatments($path, $folder_name, $sql, $content);
                }
            }
        }

        return $this->messages;
    }

    /**
     * @param string $path
     * @param string $folder_name
     * @param string $sql_file_name
     *
     * @return string
     */
    public function get_content_from_sql_file($path, $folder_name, $sql_file_name){

        $data_source = $this->get_data_source_name();

        if(file_exists($path.$folder_name.DS.$data_source.DS.$sql_file_name)){
            return file_get_contents($path.$folder_name.DS.$data_source.DS.$sql_file_name);
        } else {
            return file_get_contents($path . $folder_name . DS . $sql_file_name);
        }

    }

    /**
     * @return array
     */
    public function get_paths()
    {
        $value = Configure::read("Izzdb.paths");
        if (!empty($value) && !is_null($value)) {
            $paths = $value;
        } else {
            $paths = array(APP . ".." . DS . ".." . DS . "db" . DS);
        }

        return $paths;
    }

    /**
     * @param string $path
     * @param string $folder_name
     * @param string $sql_file_name
     * @param string $content
     */
    public function handle_multiple_sql_treaments($path, $folder_name, $sql_file_name, $content)
    {
        if (strpos($content, "#NOT_EXPLODE") !== false) {
            $this->handle_sql_treatments($path, $folder_name, $sql_file_name, $content);
        } else {
            $list_of_queries = explode(';', $content);
            foreach ($list_of_queries as $query) {
                $this->handle_sql_treatments($path, $folder_name, $sql_file_name, $query);
            }
        }
    }

    /**
     * @return bool
     */
    public function do_we_need_to_explode()
    {
        $explode = Configure::read("Izzdb.explode");
        return (!empty($explode) && $explode == true);
    }

    /**
     * @param string $path
     * @param string $folder_name
     * @param string $sql_file_name
     * @param string $query
     */
    public function handle_sql_treatments($path, $folder_name, $sql_file_name, $query)
    {
        $query = $this->prepare_sql($query);
        if (!empty($query)) {
            try {
                if ($this->check_for_pre_query_file($path, $folder_name, $sql_file_name)) {
                    $this->query($query, $folder_name, $sql_file_name);
                }

            } catch (Exception $e) {
                if ($this->handle_error($e)) {
                    $this->messages[] = "\t-" . substr($sql_file_name, 0, 30) . "... - Error: " . $e->getMessage();
                }
            }
        }
    }

    /**
     * @param string $path
     * @param string $folder_name
     * @param string $sql_file_name
     *
     * @return bool
     */
    public function check_for_pre_query_file($path, $folder_name, $sql_file_name)
    {
        $_pre_query = str_replace(".sql", "_pq.sql", $sql_file_name);

        $data_source_name = $this->get_data_source_name();

        if (
            file_exists($path . $folder_name . DS . $data_source_name . DS . $_pre_query) ||
            file_exists($path . $folder_name . DS . $_pre_query)
        ) {
            $_pre_content = $this->get_content_from_sql_file($path, $folder_name, $_pre_query);

            if (empty($_pre_content)) {
                $this->messages[] = "There isn't any content on {$_pre_query} file specified for: " . $sql_file_name;
            } else {
                $_pre_content = $this->prepare_sql($_pre_content);
                $result = $this->query($_pre_content, $folder_name, $_pre_query);
                if (count($result) == 0) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }
    }

    public function get_data_source_name(){
        return strtolower(get_class($this->getDataSource()));
    }

    /**
     * @param string $path
     * @param string $folder_name
     *
     * @return array
     */
    public function get_sql_files($path, $folder_name)
    {
        $data_source = $this->get_data_source_name();

        $folder = new Folder($path . $folder_name . DS . $data_source, false, false);

        $files = $folder->find('^((?!_pq).)*.sql$');

        if(empty($files)){
            $this->messages[] = "\t- WARNING: You should specify your datasource in your folder structure like: {$folder_name}/{$data_source}";
            $folder = new Folder($path . $folder_name, false, false);
            $files = $folder->find('^((?!_pq).)*.sql$');
        }

        asort($files);

        return $files;
    }

    /**
     * @param Exception $e
     *
     * @return bool
     */
    public function handle_error($e)
    {
        $escaped_erros = [
            "23000", // Integrity constraint violation
            "42S21", // Column already exists
            "42S01", // Base table or view already exists
            //"42000", // Syntax error or access violation
        ];

        return !in_array($e->getCode(), $escaped_erros);
    }

    /**
     * @param string $sql
     *
     * @return string
     */
    public function prepare_sql($sql)
    {
        $sql = str_replace("DELIMITER //", "", $sql);
        $sql = str_replace("DELIMITER ;", "", $sql);
        $sql = str_replace("//", ";", $sql);

        return trim($sql);
    }
}
