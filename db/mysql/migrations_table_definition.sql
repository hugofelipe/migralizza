CREATE TABLE IF NOT EXISTS migrations (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `database` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `affected_rows` int(11) DEFAULT NULL,
  `run_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `query` longtext,
  `error` text,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;